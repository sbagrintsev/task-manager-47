package ru.tsc.bagrintsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Sort {

    BY_STATUS("Sort by status"),
    BY_NAME("Sort by name"),
    BY_CREATED("Sort by date created"),
    BY_STARTED("Sort by date started");

    @NotNull
    private final String displayName;

    Sort(
            @NotNull final String displayName
    ) {
        this.displayName = displayName;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return Sort.BY_CREATED;
        for (Sort sort : Sort.values()) {
            if (sort.toString().equals(value)) {
                return sort;
            }
        }
        return null;
    }

}
