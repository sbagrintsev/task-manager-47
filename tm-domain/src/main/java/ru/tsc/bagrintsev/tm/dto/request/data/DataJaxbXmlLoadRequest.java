package ru.tsc.bagrintsev.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataJaxbXmlLoadRequest extends AbstractUserRequest {

    public DataJaxbXmlLoadRequest(@Nullable String token) {
        super(token);
    }

}
