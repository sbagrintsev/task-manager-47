package ru.tsc.bagrintsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum EntityField {
    ID("id"),
    INDEX("index"),
    NAME("name"),
    DESCRIPTION("description"),
    STATUS("status"),
    SORT("sort type"),
    PROJECT_ID("project_id"),
    TASK_ID("task id"),
    LOGIN("login"),
    PASSWORD("password"),
    EMAIL("email"),
    FIRST_NAME("first_name"),
    MIDDLE_NAME("middle_name"),
    LAST_NAME("last_name"),
    LOCKED("is_locked"),
    ROLE("role"),
    USER_ID("user_id"),
    COMMAND_NAME("name"),
    COMMAND_SHORT("short_name");

    @NotNull
    private final String displayName;

    EntityField(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
