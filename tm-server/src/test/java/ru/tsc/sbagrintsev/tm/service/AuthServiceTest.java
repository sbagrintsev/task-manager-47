package ru.tsc.sbagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserServiceDTO;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.service.AuthService;
import ru.tsc.bagrintsev.tm.service.ConnectionService;
import ru.tsc.bagrintsev.tm.service.PropertyService;
import ru.tsc.bagrintsev.tm.service.dto.ProjectServiceDTO;
import ru.tsc.bagrintsev.tm.service.dto.TaskServiceDTO;
import ru.tsc.bagrintsev.tm.service.dto.UserServiceDTO;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;

@Category(DBCategory.class)
public final class AuthServiceTest {

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private IAuthService authService;

    @Nullable
    private String token;

    @Before
    public void setUp() throws AbstractException, GeneralSecurityException {
        @NotNull final PropertyService propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);
        @NotNull final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);
        userService = new UserServiceDTO(projectService, taskService, propertyService, connectionService);
        authService = new AuthService(userService, propertyService);
        userService.create("testLogin", "testPassword");
        token = null;
    }

    @After
    public void tearDown() throws AbstractException {
        userService.removeByLogin("testLogin");
    }

    @Test
    @Category(DBCategory.class)
    public void testSignIn() throws Exception {
        Assert.assertNull(token);
        token = authService.signIn("testLogin", "testPassword");
        Assert.assertNotNull(token);
    }

    @Test
    @Category(DBCategory.class)
    public void testValidateToken() throws Exception {
        token = authService.signIn("testLogin", "testPassword");
        Assert.assertNotNull(token);
        Assert.assertNotNull(authService.validateToken(token));
    }

}
