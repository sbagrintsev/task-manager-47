package ru.tsc.bagrintsev.tm.repository.model;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.bagrintsev.tm.model.Project;

public class ProjectRepository extends UserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(
            @NotNull final EntityManager entityManager
    ) {
        super(Project.class, entityManager);
    }

}
